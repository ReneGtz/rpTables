let rpTable = class {

	/** @type { HTMLTableElement } */
	#targetElement;

	/** @type { string } */
	#targetElementID;

	/** @type { HTMLElement } */
	#targetTableBody;

	/** @type { HTMLTableRowElement } */
	#templateRowElement;

	/** @type { Array<(HTMLTableRowElement,function)[]> } */
	#toRemoveFunctions = [];

	/**
	 * represents a table and its functions in it
	 *
	 * @param tableElement { HTMLTableElement | string } the table as HTMLElement or the id of the element
	 * @param hideClass { string } the class to apply, to hide the rows
	 */
	constructor( tableElement, hideClass ){
		//get HTMLElement of the instanced table
		if( tableElement instanceof HTMLTableElement ){ //if table is HTMLElement
			this.#targetElement = tableElement;

		} else if( typeof( tableElement ) !== 'string') {
			throw new Error('First parameter "table" must be an HTMLTableElement or the id of an HTMLTableElement');

		} else { //if tableElement === string
			let element = document.getElementById( tableElement );
			if( element === null ){
				throw new Error('First parameter "tableElement" must be an HTMLTableElement or the id of an HTMLTableElement')
			}
			this.#targetElement = element;
		}
		this.#targetElementID = this.#targetElement.getAttribute( 'id' );

		if( this.#targetElement.tBodies.length !== 1 ){
			throw new Error('The given table is expected to have exactly one table-body (tbody).');
		}

		this.#targetTableBody = tableElement.tBodies[0];

		if( typeof( hideClass ) !== 'string' ){
			throw new Error('Second parameter "hideClass" must be a valid class name' );
		}

		//prepare template row
		let template = tableElement.getElementsByClassName( this.#targetElementID + '-template' );
		if( !template.hasOwnProperty( 0 ) ){
			throw new Error('Table must contain a table-row with the class of < TABLE-ID >-template.');
		}
		template = template[ 0 ];

		this.#templateRowElement = template.cloneNode(true);
		this.#templateRowElement.removeAttribute('id');
		this.#templateRowElement.classList.add( 'rpTableRow' );
		if( this.#templateRowElement.classList.contains( hideClass ) === false ){
			this.#templateRowElement.classList.add( hideClass );

		} else { //if original had hideClass
			this.#templateRowElement.classList.remove( hideClass );
		}
	} //end constructor()

	#getRowElementByID( rowID ){
		let tableRows = this.#targetElement.getElementsByClassName( 'rpTableRow' );
		for( let i in tableRows ){
			if( !tableRows.hasOwnProperty( i ) ){
				continue;
			}

			if( tableRows[ i ].dataset.id === rowID ){
				return tableRows[ i ];
			}
		}
		return null;
	}

	static #getColumnElementByID( rowElement, columnID ){
		let columns = rowElement.getElementsByTagName( 'td' );
		for( let i in columns ){
			if( !columns.hasOwnProperty( i ) ){
				continue;
			}

			if( columns[ i ].dataset.id === columnID ){
				return columns[ i ];
			}
		}
		return null;
	}

	/**
	 * add a row to the table
	 *
	 * @param data { Object<string> } an array ( object ) containing the data associated with the data-id's
	 * @param identifier { string | null } the row-id ( identifier ) of the row. ( this value will be needed for update and delete )
	 * @param onCreation { function } a function which will be executed after creation of the element ( to add on click events or whatever ). This function will gain a reference of the element as first parameter
	 * @param onRemoval { function } a function which will be executed before removing the row from the table. This function will gain a reference of the element as first parameter
	 */
	addRow( data, identifier = null, onCreation = null, onRemoval = null ){
		let newRow = this.#templateRowElement.cloneNode( true );
		newRow.setAttribute( 'data-id', identifier );
		let childNotes = newRow.children;
		for( let key in childNotes ){
			if( !childNotes.hasOwnProperty( key ) ){
				continue;
			}
			if( childNotes[ key ].dataset.id === undefined ){

				continue;
			}

			if( !data.hasOwnProperty( childNotes[ key ].dataset.id ) ){
				continue;
			}

			childNotes[ key ].textContent = data[ childNotes[ key ].dataset.id ];
			this.#targetTableBody.appendChild( newRow );
		}
		if( ( typeof onCreation) === 'function' ){
			onCreation( newRow );
		}

		if( ( typeof onRemoval ) === 'function' ){
			this.#toRemoveFunctions.push( [ newRow, onRemoval ] );
		}
	} //end function addRow

	/**
	 * add a multiple rows to the table
	 *
	 * @param data { Object<string> } an array containing arrays ( objects ) containing the data associated with the data-id's
	 * @param identifierColumn { string } the data-id of the column, which value should be used as row-id ( row identifier )
	 * @param onCreation { function } a function which will be executed after creation of the element ( to add on click events or whatever ). This function will gain a reference of the element as first parameter
	 * @param onRemoval { function } a function which will be executed before removing the row from the table. This function will gain a reference of the element as first parameter
	 */
	addRows( data, identifierColumn, onCreation = null, onRemoval = null ){
		for( let key in data ){
			if( !data.hasOwnProperty( key ) ){
				continue;
			}

			this.addRow( data[ key ], data[ key ][ identifierColumn ], onCreation, onRemoval );
		} //end foreach key
	} //end function addRows

	/**
	 * delete all rows from the table
	 */
	deleteAllRows(){
		let rows = this.#targetElement.getElementsByClassName( 'rpTableRow' );
		let i = rows.length - 1;
		while ( i > -1 ){
			this.deleteRowByElement( rows[ i ] );
			i--;
		}
	}

	/**
	 * delete the given row ( element ) from the table
	 *
	 * @param element the row to be removed
	 */
	deleteRowByElement( element ){
		for( let i in this.#toRemoveFunctions ){
			if( !this.#toRemoveFunctions.hasOwnProperty( i ) ){
				continue;
			}

			if( this.#toRemoveFunctions[ i ][ 0 ].isSameNode( element ) ){
				this.#toRemoveFunctions[ i ][ 1 ]( this.#toRemoveFunctions[ i ][ 0 ] );
				this.#toRemoveFunctions.splice( parseInt( i ), 1 );
				break;
			}
		}
		element.parentNode.removeChild( element );
	}

	/**
	 * update the value of the column associated with columnID in the row associated with rowID
	 *
	 * @param rowID the id of the row where to find the wanted column
	 * @param columnID the column id which should be updated
	 * @param newContent the content that the column should gain
	 */
	updateColumnContentByIDs( rowID, columnID, newContent ){
		let rowElement = this.#getRowElementByID( rowID );
		this.updateColumnContentByElementAndID( rowElement, columnID, newContent )
	}

	/**
	 * update the value of the column associated with the columnID in the given rowElement
	 *
	 * @param rowElement the row element containing the column to be updated
	 * @param columnID the column id which should be updated
	 * @param newContent the content that the column should gain
	 */
	updateColumnContentByElementAndID( rowElement, columnID, newContent ){
		let columnElement = rpTable.#getColumnElementByID( rowElement, columnID );
		this.updateColumnContentByElement( columnElement, newContent );
	};

	/**
	 * updates the value of the given column
	 *
	 * @param columnElement the column which content should be updated
	 * @param newContent the content that should be placed in the column
	 */
	updateColumnContentByElement( columnElement, newContent ){
		columnElement.textContent = newContent;
	};

	/**
	 * deletes a row associated with the identifier given by creation
	 *
	 * @param identifier the id of the row which was given by creation ( data-id )
	 */
	deleteRowByID( identifier ){
		let rows = this.#targetElement.getElementsByClassName( 'rpTableRow' );
		for( let i in rows ){
			if( !rows.hasOwnProperty( i ) ){
				continue;
			}

			if( rows[ i ].dataset.id !== identifier ){
				continue;
			}

			return this.deleteRowByElement( rows[ i ] );
		}
	}

}
